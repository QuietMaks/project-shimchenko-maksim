let now = new Date();
let year = now.getFullYear();
let month;
let monthArray = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
for(let i = 0; i < 12; i++){
    if(i == now.getMonth()) month = monthArray[i];
}
let day = now.getDate();
console.log(year + ' ' + month + ' ' + day);