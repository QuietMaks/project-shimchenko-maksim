let MyArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

for (let i = 0; i < 10; i++)
    if (MyArray[i] > 3 && MyArray[i] < 7) console.log(MyArray[i]);

if (CheckVariable(MyArray, 5)) console.log("Такое число есть в массиве!");
else console.log("Такого числа нет в массиве!");

SearchNumberIndex(MyArray, 5);

function CheckVariable(arr, number){
    let check = false;
    for(let i = 0; i < arr.length; i++){
        if(arr[i] === number) check = true;
    }
    return check;
}

function SearchNumberIndex(arr, number){
    let index = 0;
    for(let i = 0; i < arr.length; i++){
        if(equals(number, arr[i])){
            index = i;
            console.log("Индекс искомого числа в массиве равен: " + index);
        }
    }
}

function equals(a, b){
    return a===b;
}
